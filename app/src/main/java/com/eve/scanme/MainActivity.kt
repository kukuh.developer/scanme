package com.eve.scanme

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.eve.scanme.databinding.ActivityMainBinding
import com.github.dhaval2404.imagepicker.ImagePicker


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var imagePickerHelper: ImagePickerHelper

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                val fileUri = data?.data!!
                fileUri.path?.let {
                    binding.etCameraResult.setText(imagePickerHelper.readTextFromImage(it))
                    binding.tvResult.text = imagePickerHelper.applyArithmeticToFirstExpression(imagePickerHelper.readTextFromImage(it))
                }
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        imagePickerHelper = ImagePickerHelper(this)

        binding.btnScan.setOnClickListener {
            Toast.makeText(
                this,
                "Theme : ${BuildConfig.FLAVOR_theme} || Functionality: ${BuildConfig.FLAVOR_functionality}",
                Toast.LENGTH_SHORT
            ).show()
            when (BuildConfig.FUNCTIONALITY) {
                "BUILD_IN_CAMERA" -> {
                    ImagePicker.with(this)
                        .cameraOnly()
                        .createIntent { intent ->
                            startForProfileImageResult.launch(intent)
                        }
                }
                "FILE_SYSTEM" -> {

                }
            }
        }
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}
