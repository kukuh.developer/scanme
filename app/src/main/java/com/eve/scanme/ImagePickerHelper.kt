package com.eve.scanme

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextRecognizer

class ImagePickerHelper (private val context: Context) {
    private val textRecognizer: TextRecognizer = TextRecognizer.Builder(context).build()

    fun readTextFromImage(imagePath: String): String {
        val options = BitmapFactory.Options().apply {
            inPreferredConfig = Bitmap.Config.ARGB_8888
        }
        val bitmap = BitmapFactory.decodeFile(imagePath, options)

        if (!textRecognizer.isOperational) {
            // Text recognition is not available, handle error
            return ""
        }

        val frame = Frame.Builder().setBitmap(bitmap).build()
        val textBlocks = textRecognizer.detect(frame)

        val stringBuilder = StringBuilder()
        for (i in 0 until textBlocks.size()) {
            val textBlock = textBlocks.valueAt(i)
            stringBuilder.append(textBlock.value)
        }

        return stringBuilder.toString()
    }

    fun applyArithmeticToFirstExpression(input: String): String {
        val cleanString = input.replace(" ", "")
        val arithmeticPattern = "([\\d.]+)\\s*([+\\-*/x])\\s*([\\d.]+)".toRegex()
        var resultString = ""

        val matchResult = arithmeticPattern.find(cleanString)
        if (matchResult != null) {
            val firstExpression = matchResult.value
            val result = evaluateExpression(firstExpression)
            resultString = cleanString.replaceFirst(firstExpression, result.toString())
            return resultString
        }

        return resultString
    }

    private fun evaluateExpression(expression: String): Int {
        val characters = expression.toCharArray()
        characters.forEach {
            Log.e(TAG, "evaluateExpression: $it")
        }
        if(characters.size == 3){
            val operand1 = characters[0].toString().toInt()
            val operator = characters[1].toString()
            val operand2 = characters[2].toString().toInt()

            return when (operator) {
                "+" -> {
                    Log.e(TAG, "evaluateExpression: exp +")
                    Log.e(TAG, "evaluateExpression: op1 $operand1")
                    Log.e(TAG, "evaluateExpression: op2 $operand2")
                    operand1 + operand2
                }
                "-" -> {
                    Log.e(TAG, "evaluateExpression: exp -")
                    Log.e(TAG, "evaluateExpression: op1 $operand1")
                    Log.e(TAG, "evaluateExpression: op2 $operand2")
                    operand1 - operand2
                }
                "*" -> {
                    Log.e(TAG, "evaluateExpression: exp *")
                    Log.e(TAG, "evaluateExpression: op1 $operand1")
                    Log.e(TAG, "evaluateExpression: op2 $operand2")
                    operand1 * operand2
                }
                "/" -> {
                    Log.e(TAG, "evaluateExpression: exp /")
                    Log.e(TAG, "evaluateExpression: op1 $operand1")
                    Log.e(TAG, "evaluateExpression: op2 $operand2")
                    operand1 / operand2
                }
                "x" -> {
                    Log.e(TAG, "evaluateExpression: exp x")
                    Log.e(TAG, "evaluateExpression: op1 $operand1")
                    Log.e(TAG, "evaluateExpression: op2 $operand2")
                    operand1 * operand2
                }
                else -> {
                    Log.e(TAG, "evaluateExpression: exp else")
                    Log.e(TAG, "evaluateExpression: op1 $operand1")
                    Log.e(TAG, "evaluateExpression: op2 $operand2")
                    0
                }
            }
        } else {
            return 0
        }
    }

    companion object {
        private const val TAG = "ImagePickerHelper"
    }
}